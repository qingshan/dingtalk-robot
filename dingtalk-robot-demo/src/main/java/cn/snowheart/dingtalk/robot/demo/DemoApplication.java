package cn.snowheart.dingtalk.robot.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * DingTalk机器人的演示启动类
 *
 * @author Wanxiang Liu
 * @version 1.0.0
 */
@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

}
